package com.example.demo.database;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity
public class Department {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;

}
