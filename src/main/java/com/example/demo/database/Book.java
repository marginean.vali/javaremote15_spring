package com.example.demo.database;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "books")
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String author;
    private String isbn;
    private Integer pagesNum;

    public Book(String title, String author, String isbn, Integer pagesNum) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.pagesNum = pagesNum;
    }
}
