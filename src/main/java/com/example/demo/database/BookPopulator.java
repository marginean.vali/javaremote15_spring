package com.example.demo.database;

import com.example.demo.database.repo.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Order(value = Ordered.HIGHEST_PRECEDENCE)

public class BookPopulator implements CommandLineRunner {

    private final BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {

        Book book1 = new Book("Fellowship Of The Ring", "J.R.R.Tolkien", "123456789", 420);
        bookRepository.save(book1);
        Book book2 = new Book("Return Of The King", "J.R.R.Tolkien", "123456788", 390);
        bookRepository.save(book2);
        Book book3 = new Book("Poezii", "Mihai Eminescu", "125456789", 110);
        bookRepository.save(book3);
        Book book4 = new Book("The Hobbit", "J.R.R.Tolkien", "123356789", 150);
        bookRepository.save(book4);
        Book book5 = new Book("Poezii", "Vasile Alecsandri", "124446789", 125);
        bookRepository.save(book5);
        Book book6 = new Book("The Silmarillion", "J.R.R.Tolkien", "122226789", 285);
        bookRepository.save(book6);
    }
}
