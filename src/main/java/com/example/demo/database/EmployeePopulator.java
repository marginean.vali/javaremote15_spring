package com.example.demo.database;

import com.example.demo.database.repo.EmployeeRepository;
import com.example.demo.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class EmployeePopulator implements CommandLineRunner {

    private final EmployeeService employeeService;

    @Override
    public void run(String... args) throws Exception {

        Department department = new Department();
        department.setName("HR Department");

        department = employeeService.saveDepartment(department);

        Employee employee = new Employee();
        employee.setName("Ion");
        employee.setLastname("Pop");
        employee.setSalary(2500);
        employee.setDepartment(department);

        employeeService.saveEmployee(employee);

        Employee employee2 = new Employee();
        employee2.setName("Andrei");
        employee2.setLastname("Pop");
        employee2.setSalary(4000);
        employee2.setDepartment(department);

        employeeService.saveEmployee(employee2);

    }
}
