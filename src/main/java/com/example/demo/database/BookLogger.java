package com.example.demo.database;

import com.example.demo.database.repo.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Optional;

//@Component
@Slf4j
@RequiredArgsConstructor
public class BookLogger implements CommandLineRunner {

    private final BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {
        log.info("Books with title containing 'Poezii': " + bookRepository.findAllByTitleContaining("Poezii"));
        log.info("Books with ISBN '123356789': " + bookRepository.findByIsbn("123356789"));

        Optional<Book> bookByAuthorAndIsbn = bookRepository
                .findByAuthorAndIsbn("J.R.R.Tolkien", "123456788");
        if(bookByAuthorAndIsbn.isPresent()){
            log.info("Books with ISBN '123456788' by J.R.R.Tolkien : " + bookByAuthorAndIsbn.get());
        } else {
            log.info("No Books with ISBN '123456788' by J.R.R.Tolkien found");
        }
        log.info("3 Thickest Books by J.R.R.Tolkien: " + bookRepository.findTop3ByAuthorOrderByPagesNumDesc("J.R.R.Tolkien"));
        log.info("Books starting with 'The': " + bookRepository.findAllByTitleStartingWith("The"));
        log.info("Books with pages number between 200 and 400" + bookRepository.findAllByPagesNumBetween(200, 400));

        log.info("Books with pages number greater than 300" + bookRepository.findWherePagesNumIsGreaterThanX(300));

    }
}
