package com.example.demo.database;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
//@Table(name = "employees")
public class Employee {

    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    private String lastname;
    private Integer salary;

    @ManyToOne
    private Department department;
}
