package com.example.demo.database.repo;

import com.example.demo.database.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByTitleContaining(String title);
    Book findByIsbn(String isbn);
    Optional<Book> findByAuthorAndIsbn(String author, String isbn);
    List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author);
    List<Book> findAllByTitleStartingWith(String titlePrefix);
    List<Book> findAllByPagesNumBetween(int minPages, int maxPages);

    @Query("SELECT b FROM Book b WHERE b.pagesNum > :minPagesNum")
    List<Book> findWherePagesNumIsGreaterThanX(@Param("minPagesNum")int minPagesNum);
}
