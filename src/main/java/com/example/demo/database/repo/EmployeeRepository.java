package com.example.demo.database.repo;

import com.example.demo.database.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EmployeeRepository
        extends JpaRepository<Employee, UUID> {
    List<Employee> findAllBySalaryGreaterThan(int minSalary);

//    @Query(value = "SELECT e FROM Employee e WHERE e.salary >= :minSalary", nativeQuery = false)
//    List<Employee> findAllBySalaryGreaterThan( @Param("minSalary") int minSalary);
}
