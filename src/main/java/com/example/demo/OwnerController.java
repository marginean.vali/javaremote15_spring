package com.example.demo;

import com.example.demo.beans.Owner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Slf4j
@RequestMapping("/owner")
public class OwnerController {
    @Autowired
    private Owner owner;

    @GetMapping("/model-and-view")
    public ModelAndView getOwnerInfoModelAndView(){
        return new ModelAndView("owner-info");
    }

    @GetMapping
    public String getOwnerInfo(ModelMap modelMap){
        modelMap.addAttribute("text",
                null);
        modelMap.addAttribute("owner", owner);
        return "owner-info";
    }

    @PostMapping
    public String postOwnerInfo(Owner newOwner, ModelMap modelMap){
        owner.setName(newOwner.getName());
        owner.setLastname(newOwner.getLastname());
        owner.setEmail(newOwner.getEmail());

        modelMap.addAttribute("text",
                "Updated Owner Info");

        modelMap.addAttribute("owner", owner);
        return "owner-info";

    }

}
