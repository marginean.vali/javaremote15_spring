package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Exercise12SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/api/cars").hasAnyRole("ADMIN", "CARS")
//                .anyRequest().authenticated()
            .and()
                .formLogin()
            .and()
                .headers().frameOptions().disable()
            .and()
                .csrf().disable()
            ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}password")
                    .roles("ADMIN", "USER")
//                    .authorities("ROLE_ADMIN", "ROLE_USER")
                .and()
                .withUser("user").password("{noop}password")
                    .roles("USER");
    }

}
