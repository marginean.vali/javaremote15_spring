package com.example.demo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
public class GreetingInfo {
    @Size(min = 4)
    private String name;
}
