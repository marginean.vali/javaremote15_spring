package com.example.demo.service;

import com.example.demo.database.Department;
import com.example.demo.database.Employee;
import com.example.demo.database.repo.DepartmentRepository;
import com.example.demo.database.repo.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;

    public List<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }

    public Employee saveEmployee(Employee employee){
        return employeeRepository.save(employee);
    }

    public List<Employee> getEmployeesWithMinumumSalary(int salary){
        return employeeRepository.findAllBySalaryGreaterThan(salary);
    }

    public void deleteEmployee(UUID id){
        employeeRepository.deleteById(id);
    }

    public Department saveDepartment(Department department){
        return departmentRepository.save(department);
    }

    public Optional<Employee> getEmployeeById(UUID id){
        return employeeRepository.findById(id);
    }



}
