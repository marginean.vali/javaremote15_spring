package com.example.demo;

import com.example.demo.beans.Owner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MyCustomRunner implements CommandLineRunner {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${app.version}")
    private String applicationVersion;

    @Autowired
    private Owner owner;

    @Override
    public void run(String... args) {
        log.info("Started " + applicationName + ", v" + applicationVersion);
        log.info("Onwer is: " + owner);

    }
}
