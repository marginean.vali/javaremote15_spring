package com.example.demo;

import com.example.demo.beans.GreetingTarget;
import com.example.demo.beans.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class HelloController {

    @Autowired
    private GreetingTarget greetingTarget;

    @GetMapping(path = "/hello")
    public String sayHello(){
        return "Hello";
    }

    @GetMapping(path = "/hello2")
    public ResponseEntity<String> sayHello2(){
        return ResponseEntity.ok("Hello2");
    }

    @GetMapping(path = "/hello-target")
    public String sayHelloTarget(){
        return "Hello " + greetingTarget;
    }

    @GetMapping(path = "/hello/{name}")
    public String sayHelloWithName(@PathVariable("name") String myName,
                                   @RequestParam(value = "greeting", required = false) String greeting){
        String selectedGreeting = greeting;
        if(greeting == null){
            selectedGreeting = "Hello";
        }
        return selectedGreeting + " " + myName;
    }

    @PostMapping(path = "/hello")
    public String sayHello(@RequestBody @Validated GreetingInfo greetingInfo){
        return "Hello " + greetingInfo.getName();
    }

}
