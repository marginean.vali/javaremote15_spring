package com.example.demo;

import com.example.demo.database.Employee;
import com.example.demo.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping
    public List<Employee> getAllEmployees(){
        return employeeService.getAllEmployees();
    }

    @GetMapping("/salary/{minSalary}")
    public List<Employee> getEmployeesWithMinSalary(@PathVariable int minSalary){
        return employeeService.getEmployeesWithMinumumSalary(minSalary);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    public Employee createEmployee(@RequestBody Employee employee){
        return employeeService.saveEmployee(employee);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable UUID id){
        employeeService.deleteEmployee(id);
    }
}
