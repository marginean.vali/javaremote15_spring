package com.example.demo.beans;


import com.example.demo.MyCustomRunner;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@Component
@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = "app.owner")
@Validated
public class Owner {

    @Min(1)
    @NotNull
    private int id;
    @NotBlank
    private String name;
    @NotBlank
    private String lastname;
    @Email
    private String email;

    private List<String> permissions;

}
