package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PCGameForm {

  @Size(min = 3)
  private String title;
  private String producer;
  private String genre;

  @Min(0)
  @Max(100)
  private Integer minimumAge;
  private Boolean isAAA;
}