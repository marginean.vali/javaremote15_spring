package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@Slf4j
@RequestMapping("/pc-games")
public class PcGameController {

    @GetMapping
    public String getPcGamesFormPage(ModelMap modelMap){
        modelMap.addAttribute("createMessage", "Create PC Game");
        modelMap.addAttribute("formObj", new PCGameForm());

        return "pcgame";
    }

    @PostMapping("/submit")
    public String postPcGamesForm(
            @ModelAttribute(name="game-form") @Validated PCGameForm pcGameForm,
            ModelMap modelMap){
        modelMap.addAttribute("sentForm", pcGameForm);


        return "pcgame_info";
    }
}
