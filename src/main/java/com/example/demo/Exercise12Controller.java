package com.example.demo;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class Exercise12Controller {

    @GetMapping("/cars")
//    @PreAuthorize("hasRole('ADMIN')")
    public void getCars(){

    }

    @PostMapping("/cars")
    @PreAuthorize("isAuthenticated()")
    public void createCar(){

    }

    @GetMapping("/users")
    public void getUsers(){

    }

}
